# -*- coding: utf-8 -*-
"""
Created on Fri Jul  6 13:53:09 2018

@author: carlelg
"""


import pickle
import os

database_big = pickle.load(open("data/save.pkl", "rb" ))


path = 'data/database_split/'
for i in range(len(database_big)):
    pickle.dump(database_big[i], open('{}{}.pkl'.format(path, str(i)), "wb" ))
    print(i)