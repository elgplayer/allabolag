# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 20:37:45 2018

@author: carlelg
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Apr 15 21:27:32 2018

@author: carlelg
"""

import pickle
from datetime import datetime
import random
import os


import requests
import cfscrape
from bs4 import BeautifulSoup
import pandas as pd

from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool 

from function.misc_functions import company_scraper

from_num = 0
to_num = 1000

#database_big = pickle.load(open("data/save.pkl", "rb" ))
#database_big = database_big[from_num:to_num]

pool = ThreadPool(1000) # Sets the pool size to 4
results = pool.map(company_scraper, range(from_num, to_num))

#close the pool and wait for the work to finish 
pool.close() 
pool.join() 