# -*- coding: utf-8 -*-
import os
import pickle
from datetime import datetime
import random

import requests
import cfscrape
from bs4 import BeautifulSoup
import pandas as pd


from function.company_scraper_functions import proxy_crawler
from function.company_scraper_functions import bokslut
from function.company_scraper_functions import offentliga_varden
from function.company_scraper_functions import varumarken
from function.company_scraper_functions import verksamhet
from function.company_scraper_functions import befattninghavare
from function.company_scraper_functions import person_database
from function.company_scraper_functions import addreses




def company_scraper(value):
    
    #print(os.getcwd())
    
#
#    location = 'output/log'
#    if not os.path.exists(location):
#        os.makedirs(location)
#    
#    f = open('output/log/error.log', 'a+')
#    f2 = open('output/log/debug.log', 'a+')
    database = pickle.load(open("data/database_split/{}.pkl".format(value), "rb" ))
#    database = database[value]
    org_nr = database['org_nr']

    tmp = {}
    url = 'https://www.allabolag.se/{}/'.format(org_nr)
    

    
    #GRABS URL AND MAKES A SOUP OF IT
    a = proxy_crawler(url).text
    
    while '503 Service Unavailable' in a:
        print('503 error', org_nr)
#        print(org_nr, '503 error', file=f)
        
        a = proxy_crawler(url).text
    if 'Hittade inte företaget' in a:
        print('Hittade inte företage', org_nr)
#        print(url, file=f2)
    
    if 'Hittade inte företaget' not in a:
    
        soup = BeautifulSoup(a, 'html.parser')            
        
        tmp['org_nr'] = database['org_nr']
        tmp['kommun'] = database['kommun']
        tmp['company_type'] = database['company_type']    
        tmp['company_name'] = database['company_name'] 
        
        # TELEPHONE NUMBER
        try:
            tel_nr = soup.find('a', class_='p-tel').text
        except:
            tel_nr = None
            pass
    
        tmp['tel_nr'] = tel_nr
        
        # ADDRESES
        tmp['addreses'] = addreses(url)
        
        # EXTRACTS INNHAVARE AND THEIR HASHES
        data = soup.find('div', class_='box flex-grid__cell flex-grid__cell company-info')
        data2 = data.find_all('a', href=True)
        data2 = str(data2[0])
        data2 = data2.replace('"', '')
        data2 = data2.replace('<a href=', '')
        data2 = data2.replace('  ', '')
        data2 = data2.split('>')
        
        innehavare_url = data2[0]
        
        if data2[1][0] == ' ':
            data2[1] = data2[1][1:]
        innehavare = data2[1]
        
        innehavare = innehavare.replace('</a', '')
        
        if 'Läs mer' in innehavare:
            innehavare = None
        
        if '<a data-target-id=verksamhet' in innehavare_url:
            innehavare_url = None
        
        tmp['innehavare_url'] = innehavare_url
        tmp['innehavare'] = innehavare
            
        
        #NAVIGATION
        data = soup.find_all('nav', class_='sub-navigation box__bleed-x box__bleed-bottom company-navigation')
        data2 = data[0].find_all('li', class_='')
          
        nav = []
        for k,x in enumerate(data2):
            nav.append(str(data2[k]))
            
        for k,x in enumerate(nav):
            nav[k] = nav[k].replace('"', '')
            nav[k] = nav[k].replace('<li class=>', '')
            nav[k] = nav[k].replace('<a data-target-id=', '')
            nav[k] = nav[k].replace('\n', '')
            nav[k] = nav[k].replace('</a></li>', '')
            nav[k] = nav[k].replace('target=_self', '')
            nav[k] = nav[k].split('>')
            nav[k][0] = nav[k][0].split('=')
            nav[k][0] = nav[k][0][1]
            nav[k][0] = nav[k][0].replace(' target', '')
        
            if 'Varumärken' in nav[k][1]:
                nav[k][1] = nav[k][1][0:10]
            
            nav[k] = nav[k][1]
            
    
        for k,x in enumerate(nav):
            if nav[k] == 'Verksamhet &amp; status':
                tmp['verksamhet'] = verksamhet(url)
            
            if nav[k] == 'Offentliga värden':
                tmp['offentliga_varden'] = offentliga_varden(url)
            
            if nav[k] == 'Befattningshavare':
                tmp['befattninghavare'] = befattninghavare(url)
            
            if nav[k] == 'Varumärken':
                tmp['varumarken'] = varumarken(url)    
            
            if nav[k] == 'Bokslut &amp; nyckeltal':
                tmp['bokslut'] = bokslut(url)       
        
        
    # ADDS THE FINAL PRODUCT TO THE DATABASE
    location = 'output/{}.pkl'.format(str(value))
    pickle.dump(tmp, open(location, "wb" ) )
#    return tmp   