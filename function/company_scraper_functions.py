# -*- coding: utf-8 -*-
"""
Created on Sat Apr 21 19:41:27 2018

@author: carlelg
"""
import pickle
import random
import time
import os

import pandas as pd
import requests
import cfscrape
from fake_useragent import UserAgent
from bs4 import BeautifulSoup



# CURRENT FUNCTIONS:
#
# 1. bokslut
# 2. offentliga_varden
# 3. varumarken
# 4. verksamhet
# 5. befattninghavare
# 6  person_database



#

#%%


def proxy_crawler(url):
    
    
    ## VARIABLES ##
    max_retries = 2
    timeout = 2 # in seconds
    
    
    scraper = cfscrape.create_scraper()
    ua = UserAgent()
    text_file = open("data/ssl_proxies.txt", "r")
    proxy_list = text_file.read().split('\n')
    
    

    
    path = ('output/log/')
    if not os.path.exists(path):
        os.makedirs(path)
    
    f = open('{}proxy_error.log'.format(path), 'a+')
    
    result = None
    while result == None:
        abc = len(proxy_list) - 1
        num = random.randint(0,abc)
        for i in range(max_retries):
            try:
                boi = proxy_list[num]      
                gris = {'https': 'http://' + boi}
                headers = {'User-Agent': ua.random}
                polk = scraper.get(url, proxies=gris, 
                                   headers=headers, timeout=timeout)
                result = True
                return polk 
            
            except Exception:
                pass
        if result == None:
            print(proxy_list[num], file=f)
            del proxy_list[num]
    f.close()





#%%
###############
### BOKSLUT ###
###############
                
def bokslut(url):

    # URL CREATOR
    url3 = url + 'bokslut'
    a = proxy_crawler(url3).text
    soup = BeautifulSoup(a, 'html.parser')  
    
    # NEEDED DICTS
    fin_data = {}
    fin_data['koncern'] = {}
    fin_data['bolaget'] = {}
    xyz = {}
    abc = {} 
    table_info = {}
    
    # KONCERN   
    data = soup.find('div', class_='box box--document container__bleed-x--up-to-small')
    data2 = data.find_all('tbody', class_='')    
    
    # CREATES LIST OF LEGENDS
    tb_legend = {}
    for i in range(30):
        tb_legend[i] = (str(data.find('th', class_='data-pager__page data-pager__page--' + str(i))))
    
    # REMOVES EMPTY OBJECTS    
    for i in range(len(tb_legend)):
        if tb_legend[i] == 'None':
            del tb_legend[i]
    
    # REMOVES HTML JUNK
    for k,x in enumerate(tb_legend):
        tb_legend[k] = tb_legend[k].replace('"', '')
        tb_legend[k] = tb_legend[k].replace('<th class=data-pager__page data-pager__page--', '')
        tb_legend[k] = tb_legend[k].replace('\n', '')
        tb_legend[k] = tb_legend[k].replace('</th>', '')
        tb_legend[k] = tb_legend[k].replace(' ', '')
        tb_legend[k] = tb_legend[k].split('>')
        tb_legend[k] = tb_legend[k][1]
        
    for ko, xo in enumerate(data2):
        data3 = data2[ko].find_all('tr', class_='')
        
        for ki, xi  in enumerate(data3):
            xyz = {}
        
            # ROW NAME
            abc[ki] = data3[ki].find('th', scope='row')
            abc[ki] = str(abc[ki])
            
           
            if 'tooltiptext tooltiptext--bottom' in abc[ki]:
                # EXCEPTIONS TO "NYCKELTAL
                abc[ki] = abc[ki].split('</span>')
                abc[ki] = abc[ki][0]
                abc[ki] = abc[ki].replace('"', '')
                abc[ki] = abc[ki].replace('<th scope=row>', '')
                abc[ki] = abc[ki].replace('</th>', '')
                abc[ki] = abc[ki].replace('\n', '')
                abc[ki] = abc[ki].replace('  ', '')
                abc[ki] = abc[ki].replace('<span class=row-title>', '')
                abc[ki] = abc[ki].replace('<span class=tooltip><span>', '')
            
            
            if 'chart-marker' in abc[ki]:
                # EXCEPTIONS TO BOARD INFORMATION
                abc[ki] = abc[ki].replace('<span class=row-title has-marker>', '')
                abc[ki] = abc[ki].replace('"', '')
                abc[ki] = abc[ki].replace('<th scope=row>', '')
                abc[ki] = abc[ki].replace('</th>', '')
                abc[ki] = abc[ki].replace('\n', '')
                abc[ki] = abc[ki].replace('  ', '')
                abc[ki] = abc[ki].replace('<span class=row-title has-marker>', '')
                abc[ki] = abc[ki].replace('<span>', '')
                abc[ki] = abc[ki].replace('<span class=row-title>', '')
                abc[ki] = abc[ki].split('<')
                abc[ki] = abc[ki][0]
                
           
            else:
                # NORMAL DATA EXTRACTION
                abc[ki] = abc[ki].replace('"', '')
                abc[ki] = abc[ki].replace('<th scope=row>', '')
                abc[ki] = abc[ki].replace('</th>', '')
                abc[ki] = abc[ki].replace('\n', '')
                abc[ki] = abc[ki].replace('  ', '')
                abc[ki] = abc[ki].replace('<span class=row-title>', '')
                abc[ki] = abc[ki].replace('<span class=tooltip><span>', '')
                abc[ki] = abc[ki].replace('<span>', '')
                abc[ki] = abc[ki].replace('</span>', '')
            
            
               
            for kx, ky in enumerate(tb_legend):
                # USES NEGATIVE CLASS
                if data3[ki].find('td', class_='number--positive data-pager__page data-pager__page--' + str(kx)) == None:
                    table_info[ki] = data3[ki].find('td', class_='number--negative data-pager__page data-pager__page--' + str(kx))
                
                # USES POSTIVE CLAS
                if data3[ki].find('td', class_='number--positive data-pager__page data-pager__page--' + str(kx)) != None:
                    table_info[ki] = data3[ki].find('td', class_='number--positive data-pager__page data-pager__page--' + str(kx))
                
                xyz[kx] = str(table_info[ki])
                
                # DATA CLEANUP
                xyz[kx] = xyz[kx].replace('"', '')
                xyz[kx] = xyz[kx].replace('[<td class=number--positive data-pager__page data-pager__page--', '')
                xyz[kx] = xyz[kx].replace('</td>]', '')
                xyz[kx] = xyz[kx].split('>')
                
                if len(xyz[kx]) > 1:
                    xyz[kx] = xyz[kx][1]
                    xyz[kx] = xyz[kx].replace(' ', '')
                    xyz[kx] = xyz[kx].replace('</td', '')
                    xyz[kx] = xyz[kx].replace('\n', '')
                if xyz[kx][0] == 'None':
                    xyz[kx] = 0
                
                  
                # REPLACES KEYS WITH TABLE LEGEND
                xyz[tb_legend[kx]] = xyz.pop(ky)
        
            # ADDS THE CORRECT VALUE TO FINAL DICT
            fin_data['koncern'][abc[ki]] = xyz
            
     
    # BOLAGET       
    data = soup.find('div', class_='box box--document container__bleed-x--up-to-small')        
    data2 = data.find_all('tbody', class_='')
            
    for ko, xo in enumerate(data2):
        data3 = data2[ko].find_all('tr', class_='')
        
        for ki, xi  in enumerate(data3):
            xyz = {}
        
            # ROW NAME
            abc[ki] = data3[ki].find('th', scope='row')
            abc[ki] = str(abc[ki])
            
           
            if 'tooltiptext tooltiptext--bottom' in abc[ki]:
                # EXCEPTIONS TO "NYCKELTAL
                abc[ki] = abc[ki].split('</span>')
                abc[ki] = abc[ki][0]
                abc[ki] = abc[ki].replace('"', '')
                abc[ki] = abc[ki].replace('<th scope=row>', '')
                abc[ki] = abc[ki].replace('</th>', '')
                abc[ki] = abc[ki].replace('\n', '')
                abc[ki] = abc[ki].replace('  ', '')
                abc[ki] = abc[ki].replace('<span class=row-title>', '')
                abc[ki] = abc[ki].replace('<span class=tooltip><span>', '')
            
            
            if 'chart-marker' in abc[ki]:
                # EXCEPTIONS TO BOARD INFORMATION
                abc[ki] = abc[ki].replace('<span class=row-title has-marker>', '')
                abc[ki] = abc[ki].replace('"', '')
                abc[ki] = abc[ki].replace('<th scope=row>', '')
                abc[ki] = abc[ki].replace('</th>', '')
                abc[ki] = abc[ki].replace('\n', '')
                abc[ki] = abc[ki].replace('  ', '')
                abc[ki] = abc[ki].replace('<span class=row-title has-marker>', '')
                abc[ki] = abc[ki].replace('<span>', '')
                abc[ki] = abc[ki].replace('<span class=row-title>', '')
                abc[ki] = abc[ki].split('<')
                abc[ki] = abc[ki][0]
                
           
            else:
                # NORMAL DATA EXTRACTION
                abc[ki] = abc[ki].replace('"', '')
                abc[ki] = abc[ki].replace('<th scope=row>', '')
                abc[ki] = abc[ki].replace('</th>', '')
                abc[ki] = abc[ki].replace('\n', '')
                abc[ki] = abc[ki].replace('  ', '')
                abc[ki] = abc[ki].replace('<span class=row-title>', '')
                abc[ki] = abc[ki].replace('<span class=tooltip><span>', '')
                abc[ki] = abc[ki].replace('<span>', '')
                abc[ki] = abc[ki].replace('</span>', '')
            
            
               
            for kx, ky in enumerate(tb_legend):
                # USES NEGATIVE CLASS
                if data3[ki].find('td', class_='number--positive data-pager__page data-pager__page--' + str(kx)) == None:
                    table_info[ki] = data3[ki].find('td', class_='number--negative data-pager__page data-pager__page--' + str(kx))
                
                # USES POSTIVE CLAS
                if data3[ki].find('td', class_='number--positive data-pager__page data-pager__page--' + str(kx)) != None:
                    table_info[ki] = data3[ki].find('td', class_='number--positive data-pager__page data-pager__page--' + str(kx))
                
                xyz[kx] = str(table_info[ki])
                
                # DATA CLEANUP
                xyz[kx] = xyz[kx].replace('"', '')
                xyz[kx] = xyz[kx].replace('[<td class=number--positive data-pager__page data-pager__page--', '')
                xyz[kx] = xyz[kx].replace('</td>]', '')
                xyz[kx] = xyz[kx].split('>')
                
                if len(xyz[kx]) > 1:
                    xyz[kx] = xyz[kx][1]
                    xyz[kx] = xyz[kx].replace(' ', '')
                    xyz[kx] = xyz[kx].replace('</td', '')
                    xyz[kx] = xyz[kx].replace('\n', '')
                if xyz[kx][0] == 'None':
                    xyz[kx] = 0
                  
                # REPLACES KEYS WITH TABLE LEGEND
                xyz[tb_legend[kx]] = xyz.pop(ky)
        
            # ADDS THE CORRECT VALUE TO FINAL DICT
            fin_data['bolaget'][abc[ki]] = xyz        
    return fin_data

#%%
#########################  
### OFFENTLIGA VÄRDEN ###  
#########################    

def offentliga_varden(url):
    
#url = 'https://www.allabolag.se/5560004615/'
    
    
    
    # NEEDED DICTS 
    xyz = {}
    abc = {}         
    table_info = {}
    offentlig_data = {}
    
    url3 = url + 'offentliga-varden'     
    a = proxy_crawler(url3).text
    soup = BeautifulSoup(a, 'html.parser')  
    data = soup.find('table', class_='table--background-separator company-table')
    data2 = data.find_all('tr', class_='')
    
    # CREATES LIST OF LEGENDS
    tb_legend = {}
    for i in range(30):
        tb_legend[i] = (str(data.find('th', class_='data-pager__page data-pager__page--' + str(i))))
    
    # REMOVES EMPTY OBJECTS    
    for i in range(len(tb_legend)):
        if tb_legend[i] == 'None':
            del tb_legend[i]
    
    # REMOVES HTML JUNK
    for k,x in enumerate(tb_legend):
        tb_legend[k] = tb_legend[k].replace('"', '')
        tb_legend[k] = tb_legend[k].replace('<th class=data-pager__page data-pager__page--', '')
        tb_legend[k] = tb_legend[k].replace('\n', '')
        tb_legend[k] = tb_legend[k].replace('</th>', '')
        tb_legend[k] = tb_legend[k].replace(' ', '')
        tb_legend[k] = tb_legend[k].split('>')
        tb_legend[k] = tb_legend[k][1]
        
    
    for ki, xi in enumerate(data2):
        xyz = {}
        
        # REMOVES WRONG DATA
        if ki == 0:
            ki += 1
        
        # REMOVES BLANK ROW
        if ki == 3:
            ki += 1
        abc[ki] = {}
        data3 = data2[ki]
        
        # ROW NAME
        abc[ki] = data3.find('th', scope='row')
        abc[ki] = str(abc[ki])
        
        if 'tooltiptext tooltiptext--bottom' in abc[ki]:
            # EXCEPTIONS TO "NYCKELTAL
            abc[ki] = abc[ki].split('</span>')
            abc[ki] = abc[ki][0]
            abc[ki] = abc[ki].replace('"', '')
            abc[ki] = abc[ki].replace('<th scope=row>', '')
            abc[ki] = abc[ki].replace('</th>', '')
            abc[ki] = abc[ki].replace('\n', '')
            abc[ki] = abc[ki].replace('  ', '')
            abc[ki] = abc[ki].replace('<span class=row-title>', '')
            abc[ki] = abc[ki].replace('<span class=tooltip>', '')
            abc[ki] = abc[ki].split('<')
            abc[ki] = abc[ki][0]
            
        else:
           abc[ki] = abc[ki].replace('"', '')
           abc[ki] = abc[ki].replace('<th scope=row>', '')
           abc[ki] = abc[ki].replace('<a href=', '')
           abc[ki] = abc[ki].replace('</th>', '')
           abc[ki] = abc[ki].replace('\n', '')
           abc[ki] = abc[ki].replace('  ', '')
           abc[ki] = abc[ki].replace('</a>', '|')
           abc[ki] = abc[ki].split('|')
           abc[ki] = abc[ki][0]
           
           if 'https://' in abc[ki]:
               abc[ki] = abc[ki].split('>') 
               abc[ki] = abc[ki][1] + '|' + abc[ki][0]
             
           
        for kx, ky in enumerate(tb_legend):
            
            # USES NEGATIVE CLASS
            if data3.find('td', class_='number--positive data-pager__page data-pager__page--' + str(kx)) == None:
                table_info[ki] = data3.find('td', class_='number--negative data-pager__page data-pager__page--' + str(kx))
            
            # USES POSTIVE CLAS
            if data3.find('td', class_='number--positive data-pager__page data-pager__page--' + str(kx)) != None:
                table_info[ki] = data3.find('td', class_='number--positive data-pager__page data-pager__page--' + str(kx))
            
            xyz[kx] = str(table_info[ki])
            
            
            # DATA CLEANUP
            if xyz[kx] == None:
                 xyz[kx] = 0
            if xyz[kx][0] == 'None':
                xyz[kx] = 0
                
            try:
                xyz[kx] = xyz[kx].replace('"', '')
                xyz[kx] = xyz[kx].replace('[<td class=number--positive data-pager__page data-pager__page--', '')
                xyz[kx] = xyz[kx].replace('</td>]', '')
                xyz[kx] = xyz[kx].split('>')
                xyz[kx] = xyz[kx][1]
                xyz[kx] = xyz[kx].replace(' ', '')
                xyz[kx] = xyz[kx].replace('</td', '')
                xyz[kx] = xyz[kx].replace('\n', '')
            except:
                xyz[kx] = 0
                
              
            # REPLACES KEYS WITH TABLE LEGEND
            xyz[tb_legend[kx]] = xyz.pop(ky)
        
        # ADDS THE CORRECT VALUE TO FINAL DICT
        offentlig_data[abc[ki]] = xyz
    
    return offentlig_data

#%%
    
##################
### VARUMÄRKEN ###
##################    

def varumarken(url):
    
    
    
    
    # URL CREATOR
    url3 = url + 'varumarken/alla'
    a = proxy_crawler(url3).text
    soup = BeautifulSoup(a, 'html.parser')  
    data = soup.find_all('div', class_='flex-grid__cell brand-item')
    
    # NEEDED DICTS
    varumarken = {}
    
    for ki,xi in enumerate(data):
        varumarken[ki] = {}
        data2 = data[ki]   
        
        # EXTRACT IMAGE LINK
        img_url = str(data2.find('img'))
        img_url = img_url.replace('"', '')
        img_url = img_url.replace('<img src=', '')
        img_url = img_url.replace('/>', '')
        
        # EXTRACT LINK AND NAME
        varumark_url = str(data2.find('a'))
        varumark_url = varumark_url.replace('"', '')
        varumark_url = varumark_url.replace('<a href=', '')
        varumark_url = varumark_url.replace('/>', '')
        varumark_url = varumark_url.replace('</a>', '')
        varumark_url = varumark_url.split('>')
        varumark_namn = varumark_url[1]
        varumark_url = varumark_url[0]
        
        # EXTREACTS TABLE DATA
        tmp = str(data2.find_all('tr'))
        tmp = tmp.replace('"', '')
        tmp = tmp.replace('<th scope=row>', '')
        tmp = tmp.replace('<tr', '')
        tmp = tmp.replace('[>', '')
        tmp = tmp.replace('\n', '')
        tmp = tmp.replace('</th><td class=', '') 
        tmp = tmp.replace(' >', '')
        tmp = tmp.replace(']', '')
        tmp = tmp.replace('</td></tr>', '')
        tmp = tmp.split(',')
        
        # SPLITS TABLE DATA TO KEYS AND VALUES
        tmp[0] = tmp[0].split('>')
        tmp[1] = tmp[1].split('>')
        tmp[2] = tmp[2].split('>')
        tmp[3] = tmp[3].split('>')
        
        varumarken[ki]['varumark_namn'] = varumark_namn
        varumarken[ki]['varumark_url'] = varumark_url
        varumarken[ki]['img_url'] = img_url
         
        
        # INSERTS EXTRACTED TABLE DATA
        varumarken[ki][[tmp][0][0][0]] = [tmp][0][0][1] 
        varumarken[ki][[tmp][0][1][0]] = [tmp][0][1][1] 
        varumarken[ki][[tmp][0][2][0]] = [tmp][0][2][1] 
        varumarken[ki][[tmp][0][3][0]] = [tmp][0][3][1] 
        
    return varumarken

#%%
    
##################
### VERKSAMHET ###
##################        

def verksamhet(url):
    
    
    
    default_url = 'https://www.allabolag.se/'
    
    # URL CREATOR
    url3 = url + 'verksamhet'
    
    a = proxy_crawler(url3).text
    soup = BeautifulSoup(a, 'html.parser')
    data = soup.find('div', class_='organization')

    
    #NEEDED DICTS:
    status = {}
    verksamhet = {}
    bransch = {}
    bransch['name'] = {}
    bransch['url'] = {}
    indelning = {}   
    
    ### FINDS VERKSAMHET BESKRIVNING ###
    try:
        verksamhet_besk = str(data.find('p', class_='accordion-body display-none'))
        verksamhet_besk = verksamhet_besk.replace('"', '')
        verksamhet_besk = verksamhet_besk.replace('<p class=accordion-body display-none>' ,'')
        verksamhet_besk = verksamhet_besk.replace('\n', '')
        verksamhet_besk = verksamhet_besk.replace('  ', '')
        verksamhet_besk = verksamhet_besk.replace('</p>', '')
    except:
         verksamhet_besk = None
         pass
    
    ### FINDS STATUS DATA ###
    data2 = soup.find('dl', class_='accordion-body display-none') 
    tmp = str(data2)
    tmp = tmp.split('<br/>')
    
    for k,x in enumerate(tmp):
        
        # CLEANS UP THE DATA
        tmp[k] = tmp[k].replace('"', '')
        tmp[k] = tmp[k].replace('<dl class=accordion-body display-none>', '')
        tmp[k] = tmp[k].replace('\n', '')
        tmp[k] = tmp[k].replace('<dt>', '')
        tmp[k] = tmp[k].replace('<dd>', '')
        tmp[k] = tmp[k].split('</dd>')
        
        for ky, kx in enumerate(tmp[k]):
            tmp[k][ky] = tmp[k][ky].split('</dt>')
        
        # REMOVES EMPTY LIST FROM ALL ITEMS
        tmp[k] = tmp[k][:-1]
        
        # APPENDS THE LISTS TO DICTIONARY    
        for ky, kx in enumerate(tmp[k]):
            try:
                status[tmp[k][ky][0]] = tmp[k][ky][1]
            except:
                pass
    
    # REPLACES DOUBLE SPACE    
    for k, x in enumerate(status):
        status[x] = status[x].replace('  ', '')   
        
        # REMVOES SPACES IN THE BEGINNING
        if status[x][0] == ' ':
            status[x] = status[x][1:] 
        
        # DISCARDS URL
        if 'href' in status[x]:
            status[x] = status[x].replace('<a href=/', '')
            status[x] = status[x].replace('</a>', '')
            status[x] = status[x].split('>') # DISCARDS URL LINK, NEEDS FIX @need_fix
            status[x] = status[x][1] + '|' + default_url + status[x][0]
    
    ### FINDS BRANCH DATA ###
    data2 = soup.find('ul', class_='accordion-body display-none tree margin-none')
    data3 = str(data2.find('li', class_=''))
    
    # CLEANS UP THE DATA
    data3 = data3.replace('"', '')
    data3 = data3.replace('\n', '')
    data3 = data3.replace('<li><a href=/', '')
    data3 = data3.replace('</a></li></ul></li>', '')
    data3 = data3.replace('<ul>' ,'')
    data3 = data3.split('</a>')
    
    # SPLITS THE DATA ON URL AND NAME
    for k,x in enumerate(data3):
        data3[k] = data3[k].split('>')
        bransch['name'][k] = data3[k][1]
        bransch['url'][k] = data3[k][0]
        
    # FIXES AND DOES FINAL CLEAN UP OF THE DATA
    for k,x in enumerate(bransch['name']):
        bransch['name'][k] = bransch['name'][k].replace('\n', '')
        bransch['name'][k] = bransch['name'][k].replace('  ', '')
        bransch['url'][k] = url + bransch['url'][k]
        
    
    ### FINDS NÄRINGGRENDSINDELING ###
    data2 = soup.find('dl', class_='accordion-body display-none sni')
    
    # CLEANS THE DATA
    tmp = str(data2)
    tmp = tmp.replace('"', '')
    tmp = tmp.replace('<dl class=accordion-body display-none sni>', '')
    tmp = tmp.replace('<dd>', '')
    tmp = tmp.replace('<dt>', '')
    tmp = tmp.replace('</a>', '')
    tmp = tmp.replace('</dt>', '')
    tmp = tmp.replace('<a href', '')
    tmp = tmp.split('</dd>')
    tmp = tmp[:-1]
    
    # SPLITS AND APPENDS THE DATA
    for k,x in enumerate(tmp):
        indelning[k] = {}
        tmp[k] = tmp[k].replace('\n', '')
        tmp[k] = tmp[k].split('=') 
        tmp[k][1] = tmp[k][1].split('>')
        
        indelning[k]['sni_id'] = tmp[k][0]
        indelning[k]['name'] = tmp[k][1][1]
        indelning[k]['url'] = tmp[k][1][0]
        
    # APPENDS DICT TO FINAL DICT
    verksamhet['Verksamhet & ändamål'] = verksamhet_besk
    verksamhet['Status'] = status
    verksamhet['Branch'] = bransch
    verksamhet['Svensk näringsgrensindelning'] = indelning
    
    return verksamhet

#%%
    

########################
### BEFATNINGSHAVARE ###
########################

def befattninghavare(url):
    
    
    
    
    # URL CREATOR
    url3 = url + 'befattningar'
    a = proxy_crawler(url3).text
    soup = BeautifulSoup(a, 'html.parser')
    
    # NEEDED DICTS
    befattningar = {}
    data5 = {}
    tmp = {}
    
    data = soup.find('div', class_='box executives')
    data2 = data.find_all('div', class_='flex-grid__cell accordion__cell')
    
    for k,x in enumerate(data2):
        data3 = data2[k]
        
        # GETS HEADER
        header = str(data3.find('h3', class_='h2 executives__column-title accordion-head'))
        header = header.replace('"', '')
        header = header.replace('<h3 class=h2 executives__column-title accordion-head>', '')
        header = header.replace('<span class="icon icon--chevron-down"></span>', '')
        header = header.replace('</h3>', '')
        header = header.replace('<span class=icon icon--chevron-down></span>', '')
        header = header.replace('\n', '')
        header = header.replace('  ', '')
        header = header.split('(')
        header = header[0]
        
        befattningar[header] = {} 
        data4 = data3.find_all('div', class_='list--personnel__info')
        
        for ky, kx in enumerate(data4):
            
            # EXTRACTS THE DATA
            tmp[ky] = {}
            tmp[ky] = str(data4[ky])
            tmp[ky] = tmp[ky].replace('"', '')
            tmp[ky] = tmp[ky].replace('<div class=list--personnel__info>', '')
            tmp[ky] = tmp[ky].replace('</br></div>', '')
            tmp[ky] = tmp[ky].replace('<a href=', '')
            tmp[ky] = tmp[ky].replace('<br>', '')
            tmp[ky] = tmp[ky].replace('\n', '')
            tmp[ky] = tmp[ky].split('</a>')
            
            # SPLITS AND KEEP THE GOOD STUFF
            tmp[ky][0] = tmp[ky][0].split('>')
            tmp[ky][0][1] = tmp[ky][0][1].split('(')
            tmp[ky][0][1] = tmp[ky][0][1][0]
            tmp[ky][0][1] = tmp[ky][0][1].replace('  ', '')
            if tmp[ky][0][1][0] == ' ':
                tmp[ky][0][1] = tmp[ky][0][1][1:]
            
            tmp[ky][1] =tmp[ky][1].replace('<br/>', '')
            tmp[ky][1] =tmp[ky][1].replace('</div>', '')
            tmp[ky][1] = tmp[ky][1].replace('  ', '')
            tmp[ky][1] = tmp[ky][1].split(',')
            tmp[ky][1] = tmp[ky][1][0]
            
            # SPLIT THE DATA TO GET THE HASH
            boi = tmp[ky][0][0].split('/')
        
            befattningar[header][ky] = {}
            befattningar[header][ky]['name'] = tmp[ky][0][1]
            befattningar[header][ky]['roll'] = tmp[ky][1]
            befattningar[header][ky]['url'] = tmp[ky][0][0]
            
            # ADDS HASH AND DETERMINES IF PERSON OR NOT
            if len(boi) > 5:
                befattningar[header][ky]['hash'] = boi[5]
                befattningar[header][ky]['person'] = 1
            
            else:
                befattningar[header][ky]['person'] = 0
    
        # EXCEPTION TO FIRMATECKNAR CATEGORY
        if header == 'Firmatecknare':
            data4 = data3.find('ul', class_='accordion-body')
            data4 = data4.find_all('li')
            
            for ku,xu in enumerate(data4):
                data5[ku] = str(data4[ku])
                data5[ku] = data5[ku].replace('<li>', '')
                data5[ku] = data5[ku].replace('</li>', '')
                data5[ku] = data5[ku].replace('\n', '')
                data5[ku] = data5[ku].replace('  ', '')
            befattningar[header] = data5
    return befattningar

#%%
    
########################
### PERSON_DATABASE ###
########################

def person_database(url):
    
    
    
    
    # NEEDED DICTS
    befattningar = {}
    data5 = {}
    tmp = {}
    person = {}
    
    # URL CREATOR
    url3 = url + 'befattningar'
    a = proxy_crawler(url3).text
    soup = BeautifulSoup(a, 'html.parser')
    
    
    data = soup.find('div', class_='box executives')
    data2 = data.find_all('div', class_='flex-grid__cell accordion__cell')
    
    for k,x in enumerate(data2):
        data3 = data2[k]
        
        # GETS HEADER
        header = str(data3.find('h3', class_='h2 executives__column-title accordion-head'))
        header = header.replace('"', '')
        header = header.replace('<h3 class=h2 executives__column-title accordion-head>', '')
        header = header.replace('<span class="icon icon--chevron-down"></span>', '')
        header = header.replace('</h3>', '')
        header = header.replace('<span class=icon icon--chevron-down></span>', '')
        header = header.replace('\n', '')
        header = header.replace('  ', '')
        header = header.split('(')
        header = header[0]
        
        befattningar[header] = {} 
        data4 = data3.find_all('div', class_='list--personnel__info')
        
        for ky, kx in enumerate(data4):
            
            # EXTRACTS THE DATA
            tmp[ky] = {}
            tmp[ky] = str(data4[ky])
            tmp[ky] = tmp[ky].replace('"', '')
            tmp[ky] = tmp[ky].replace('<div class=list--personnel__info>', '')
            tmp[ky] = tmp[ky].replace('</br></div>', '')
            tmp[ky] = tmp[ky].replace('<a href=', '')
            tmp[ky] = tmp[ky].replace('<br>', '')
            tmp[ky] = tmp[ky].replace('\n', '')
            tmp[ky] = tmp[ky].split('</a>')
            
            # SPLITS AND KEEP THE GOOD STUFF
            tmp[ky][0] = tmp[ky][0].split('>')
            tmp[ky][0][1] = tmp[ky][0][1].split('(')
            tmp[ky][0][1] = tmp[ky][0][1][0]
            tmp[ky][0][1] = tmp[ky][0][1].replace('  ', '')
            if tmp[ky][0][1][0] == ' ':
                tmp[ky][0][1] = tmp[ky][0][1][1:]
            
            tmp[ky][1] =tmp[ky][1].replace('<br/>', '')
            tmp[ky][1] =tmp[ky][1].replace('</div>', '')
            tmp[ky][1] = tmp[ky][1].replace('  ', '')
            tmp[ky][1] = tmp[ky][1].split(',')
            tmp[ky][1] = tmp[ky][1][0]
            
            # SPLIT THE DATA TO GET THE HASH
            boi = tmp[ky][0][0].split('/')
        
            befattningar[header][ky] = {}
            befattningar[header][ky]['name'] = tmp[ky][0][1]
            befattningar[header][ky]['roll'] = tmp[ky][1]
            befattningar[header][ky]['url'] = tmp[ky][0][0]
            
            # ADDS HASH AND DETERMINES IF PERSON OR NOT
            if len(boi) > 5:
                befattningar[header][ky]['hash'] = boi[5]
                befattningar[header][ky]['person'] = 1
            
            else:
                befattningar[header][ky]['person'] = 0
    
        if len(befattningar[header]) == 0:
            del befattningar[header]
          
    # APPENDS THE PEOPLE TO LIST        
    for k,x in enumerate(befattningar):
        for kx, ki in enumerate(befattningar[x]):
            if befattningar[x][kx]['person'] == 1:
                person[befattningar[x][kx]['hash']] = {}
                person[befattningar[x][kx]['hash']]['name'] = befattningar[x][kx]['name']
                person[befattningar[x][kx]['hash']]['roll'] = befattningar[x][kx]['url']
                person[befattningar[x][kx]['hash']]['url'] = befattningar[x][kx]['name']
                person[befattningar[x][kx]['hash']]['hash'] = befattningar[x][kx]['hash']
                person[befattningar[x][kx]['hash']]['bolag'] = {}
                person[befattningar[x][kx]['hash']]['bolag'] ['bolag'] = url
    return person

#%%
    
################
### ADDRESES ###
################

def addreses(url):
    
#    url = 'https://www.allabolag.se/740507PNJD/'
    
     
    
    
    url3 = url + 'adresser'
    a = proxy_crawler(url3).text
    soup = BeautifulSoup(a, 'html.parser') 
    
    lok = {}
    lok['Bolagsadresser'] = []
    lok['Adresser till arbetsställen'] = []
    boll = {}
    
    
    soup2 =  soup.find('div', class_='page__main')
    headers = soup2.find_all('h4', class_='margin-none') 
    tags = soup2.find_all(['h2', 'div'])
    
    for k,x in enumerate(tags):
        boll[k] = str(tags[k])
        
    del boll[0]
    del boll[1]
    del boll[2]
    
    
    boll2 = {}
    boll3 = {}
    
    
    # EXTRACTS THE RELEVANT INFO TO A DICTIONARY
    for k,x in enumerate(boll):
        boll[x] = boll[x].replace('"', '')
        
        if 'Utdelningsadress' in boll[x] or 'href' in boll[x] or 'h2' in boll[x]:
            boll2[x] = boll[x]
        
    # CLEANS THE START AND STOP ELEMENTS TEXT UP
    for k,x in enumerate(boll2):
        boll2[x] = boll2[x].replace('<h2>', '')
        boll2[x] = boll2[x].replace('</h2>', '')
        boll2[x] = boll2[x].replace('<h2 class=>', '')
        boll2[x] = boll2[x].replace('</h2 class=>', '')
    
    # FINDS START AND STOP ELEMENTS
    for k,x in enumerate(boll2):
        if boll2[x] == 'Bolagsadresser':
            start = x
    #        
        if boll2[x] == 'Adresser till arbetsställen':
            stop = x
    
    # REMOVES START AND STOP ELEMENTS        
    del boll2[start]
    del boll2[stop]
    
    # REMOVE DUPLICATES IN DICTIONARY    
    for k,x in enumerate(boll2):
        if x % 2 == 0:
            boll3[x] = x
    
    
    # ACTUAL LOOP TO GET THE DATA
    for kx,ky in enumerate(boll3):
        # DATA UNDER BOLAGS ADDRESER
        if ky <= stop:
            addres_bo = 1
            tmp3 = {}
            tel_nr = None
            address = None
           
            # ADDRES TP
            address_tp = tags[ky].find('h4').text # ADDRES TYPE
            
            try:
                # ADDRESS
                address = tags[kx].find('a', class_='desktop-only').text
                address = address.replace('  ', '')
                address = address.replace('\n', '')
            except:
                pass
                
            # GOOGLE LINK
            google_link = str(tags[ky].find('a', class_='desktop-only'))
            google_link = google_link.replace('"', '')
            google_link = google_link.replace('<span class=icon--right-s icon icon--place>', '')
            google_link = google_link.replace('\n', '')
            google_link = google_link.replace('<a class=desktop-only', '')
            google_link = google_link.replace('</a>', '')
            google_link = google_link.replace('</span>', '')
            google_link = google_link.replace(' href=', '')
            google_link = google_link.split('target=')
            google_link = google_link[0]
            
            
            # APPENDS INFO TO FINAL DICT
            address_tp = None
            address = None
            google_link = None
            post_nr = None
            ort = None
            region = None
            tel_nr = None
            
           
            
            kry = []
            zucc = tags[ky].text
            zucc = zucc.replace('  ', '')
            zucc = zucc.split('\n')       
            
            # APPENDS MATCHES TO LIST
            for k,x in enumerate(zucc):
                if zucc[k] != '':
                    kry.append(zucc[k])
            
            
            
            # EXTRACT INFO
            if addres_bo == 0:
                address = kry[1]
                
            # EXTRACTS ADDRES INFO
            plain_text_address = kry[1]
            kry[2] = kry[2].split(' ')
            
            try:
                post_nr = kry[2][0]
            except:
                post_nr = None
                pass
            
            try:
                ort = kry[2][1]
            except:
                ort = None
                pass
        
            
            try:
                region = tags[ky].find('span', class_='p-region').text
                
            except:
                region =  kry[3]
            
            else:
                region = None
                pass
            
            if len(kry) > 4:
                tel_nr = kry[4]      
            else:
                tel_nr = None
        
            
            
            # EXTRACTS REGION INFO
            try:
                post_nr = tags[ky].find('span', class_='p-postal-code').text 
            except:
                post_nr = kry[2][0]
            else:
                post_nr = None
                pass
            
            try:
                ort = tags[0].find('span', class_='p-locality').text 
                if ort[0] == ' ':
                    ort = ort[1:]
            except:
                ort = None
                pass

                
            
            
            # APPENDS INFO TO FINAL DICT
            tmp3['address_tp'] = address_tp
            tmp3['address'] = address
            tmp3['google_link'] = google_link
            tmp3['post_nr'] = post_nr
            tmp3['ort'] = ort
            tmp3['region'] = region
            tmp3['tel_nr'] = tel_nr
            
            # APPENDS TO OUTPUT DICT
            lok['Bolagsadresser'].append(tmp3)   
        
        # DATA UNDER ARBETSTILLFÄLLEN
        if ky >= stop:
            
            tel_nr = None
            tmp3 = {}  
            
            # ADDRESS TYPE
            address_tp = tags[ky].find('h4').text 
            
            # FINDS ADDRESS
            try:
                address = tags[ky].find('a', href=True).text
                address = address.replace('  ', '')
                address = address.replace('\n', '')
            except:
                addres_bo = 0
                pass
            
            # GOOGLE LINK
            google_link = str(tags[ky].find('a', href=True))
            google_link = google_link.replace('"', '')
            google_link = google_link.replace('<a href=', '')
            google_link = google_link.split('target=')
            google_link = google_link[0]
            
            kry = []
            zucc = tags[ky].text
            zucc = zucc.replace('  ', '')
            zucc = zucc.split('\n')       
            
            # APPENDS MATCHES TO LIST
            for k,x in enumerate(zucc):
                if zucc[k] != '':
                    kry.append(zucc[k])
            
            
            # EXTRACT INFO
            addres_bo = None
            if addres_bo == 0:
                address = kry[1]
                
            # EXTRACTS ADDRES INFO
            plain_text_address = kry[1]
            kry[2] = kry[2].split(' ')
            post_nr = kry[2][0]
            ort =  kry[2][1]
            
            try:
                region =  kry[3]
            except:
                region = None
                pass
            
            if len(kry) > 4:
                tel_nr = kry[4]      
            else:
                tel_nr = None
            
            address = None
            
            # APPENDS INFO TO FINAL DICT
            tmp3['address_tp'] = address_tp
            tmp3['address'] = address
            tmp3['google_link'] = google_link
            tmp3['post_nr'] = post_nr
            tmp3['ort'] = ort
            tmp3['region'] = region
            tmp3['tel_nr'] = tel_nr
            tmp3['plain_address'] = plain_text_address
            lok['Adresser till arbetsställen'].append(tmp3)
    return lok
#        tmp3['addres_nanmn'] = headers[counter].text
