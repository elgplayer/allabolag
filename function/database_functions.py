
import requests
from bs4 import BeautifulSoup
import pandas as pd

# QUERY VARIBLES
search_options = {'tp': 0, 'bransch': 1, 'omsattning': 2, 'ort': 3}
search_options_url = {0: 'xb', 1: 'xv', 2: 'xo', 3: 'x1'}
bolags_categories = pd.read_csv('data/bolags_categories.txt', sep='|', encoding='utf-8')

# SCRAPER
def scraper(url, category):
    
    # REQUESTS URL
    a = requests.get(url).text
    soup = BeautifulSoup(a, 'html.parser')
    
    # FINDS THE TAG IN HTML
    #kaka = soup.find('li', class_='search-filter__prop')
    boi = soup.find_all('span', class_='test')
    boi2 = soup.find_all('label', class_='filter-label')
    
    # SPLITS AND REMOVES CLUTTER FROM HITS
    hits = soup.find('span', class_='search-results__hit-count font-weight--normal').text
    hits = hits.replace(' ', '')
    hits = hits.replace('\n', '')
    hits = hits.replace('träffar', '')
    
    # CONVERTS HITS TO INT
    try:
        hits = int(hits)
    except:
        pass
    
    #EXTRACTS THE TEXT
    lok = {}    
    split = []
    for k,x in enumerate(boi):
        split.append(boi2[k].text)
        lok[k] = {}
    
    #REMOVES ANNOYING SPACES
    for k,x in enumerate(boi):
        split[k] = split[k].replace(' ', '')
        split[k] = split[k].replace('\n', ' ')
        split[k] = split[k].replace('(', '')
        split[k] = split[k].replace(')', '')
        split[k] = split[k].split(' ')
        try:
            split[k][1] = int(split[k][1])
        except:
            pass
        del split[k][2]    
    
    #CREATES DICTIONARY FROM LIST
    for k,x in enumerate(split):
        lok[k][0] = split[k][0]
        lok[k][1] = split[k][1]
    
    # ADDS CATEGORY AND URL
    for k,x in enumerate(lok):
        for index, row in bolags_categories.iterrows():
            if lok[k][0] == row['human']:
                lok[k][2] = (row['category'])
                lok[k][3] = (row['url'])
    
    # REMOVES ITEMS IF NOT CATEGORY 0
    for i in range(len(lok)):
        try:
            while len(lok[i]) < 3:
                del lok[i]
            if lok[i][2] != category:
                del lok[i]
        except:
            pass
        
    # REINDEX DICTIONARY       
    lok = {i: v for i, v in enumerate(lok.values())}
    
    #CREATES A LIST TO RETURN VALUE FROM FUCNTION     
    result = []
    result.append(hits)
    result.append(lok)
    
    return result


# GENERATES PAGES
def page_generator(result, url):
    element = {}
    element = result
    
    for ki, xi in enumerate(element[1]):
        if element[1][ki][1] < 8000:
            if len(element[1][ki]) > 3:
                #if element[search_item][1][ki][2] == 0:
                #for k,x in enumerate(element[search_item][1]):
                element[1][ki]['pages'] = {}
                pages = element[1][ki][1]//20 + 1
                
                #for kx, ky in enumerate(search_options_url):
                for i in range(pages):
                    options_url = element[1][ki][2]
                    category_url = element[1][ki][3]
    
                    element[1][ki]['pages'][i] = url + '/' + search_options_url[options_url] + '/' + category_url + '/page/' + str(i + 1)
    return element
    
    

















