# -*- coding: utf-8 -*-
"""
Created on Tue May 29 14:40:02 2018

@author: carlelg
"""

import os
import pickle

def missing(range1, range2):
    boi = {}

    range1 = str(range1)
    range2 = str(range2)

    # Creates a list of files in output directory
    files = list(os.listdir('output/' +  range1 + '-' + range2))
    
    f = open('output/' + range1 + '-' + range2  + '/log/missing.txt', 'w')

    # Removes files if not a PKL file
    for i in range(len(files)):
        try:
            if '.pkl' not in files[i]:
                del files[i]
        except:
            pass
    
    # Adds key to dictionary        
    for k,x in enumerate(files):
        key = files[k].replace('.pkl', '')
        key = int(key)
        boi[key] = files[k]
        
    # Checks for key in dictionary, if not print missing key to file
    for k,x in enumerate(files):
        try:
            boi[k]
        except:
            print(k, file=f)
            pass   

    f.close()