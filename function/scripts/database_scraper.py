# -*- coding: utf-8 -*-
"""
Created on Sun Apr 15 17:18:20 2018

@author: carlelg
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Apr 15 14:38:12 2018

@author: carlelg
"""

import pickle
import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
import time

import requests
from bs4 import BeautifulSoup
import pandas as pd

#urls = pickle.load(open( "data/allabolag_url.pkl", "rb" ))
urls = pickle.load(open( "data/search/combined.pkl", "rb" ))
url_str = 'https://www.allabolag.se/'
output = []
output2 = {}
failed = []
urls = urls[:4000]

#%%



#for kx, ky in enumerate(urls):
def scraper(url):
    check = 0
    database = {}
    index = 0
    boi = []
#    url = urls[kx]
    
    a = requests.get(url).text
    soup = BeautifulSoup(a, 'html.parser')  
    data = soup.find_all('h2', class_='search-results__item__title')
    
#    if data == None:
#        time.sleep(5)
#        a = requests.get(url).text
#        soup = BeautifulSoup(a, 'html.parser')  
#        data = soup.find_all('h2', class_='search-results__item__title')
#    
    # CREATES STR OF TAG OBJECT
    for k,x in enumerate(data):
        boi.append(str(data[k]))
    
    # CLEANS UP THE DATA
    for k,x in enumerate(boi):
        boi[k] = boi[k].replace('"', '')
        boi[k] = boi[k].replace('<h2 class=search-results__item__title>', '')
        boi[k] = boi[k].replace('<a href=https://www.allabolag.se/', '')
        boi[k] = boi[k].replace('</a>', '')
        boi[k] = boi[k].replace('</h2>', '')
        boi[k] = boi[k].split('/')
        boi[k][0] = boi[k][0].replace('\n', '')
        boi[k][1] = boi[k][1].split('>')
        boi[k].append(boi[k][1][0])
        boi[k].append(boi[k][1][1])
        del boi[k][1]
        boi[k][2] = boi[k][2].replace('\n', '')
    
    # CREATES NESTED DICTIONARY
    for k,x in enumerate(boi):
        database[index] = {}
        database[index]['org_nr'] = boi[k][0]
        database[index]['url_name'] = boi[k][1]
        database[index]['name'] = boi[k][2]
        database[index]['url'] = url_str + database[index]['org_nr'] + '/' + database[index]['url_name'] 
        index += 1
        
    if bool(database) == True:
        check = 1
    
    while check != 1:    
        if bool(database) == False:
            database = scraper2(url)
            check += 0.2
        if bool(database) == True:
            check = 1
#    if check == 0:
#        database = scraper2(url)            
#
#        if bool(database) == True:
#            check = 1
#        if bool(database) == False:
#            database = scraper2(url)
#            check = 1
            

    return database


def scraper2(url):
#    time.sleep(3)
    database = {}
    index = 0
    boi = []
#    url = urls[kx]
    
    a = requests.get(url).text
    soup = BeautifulSoup(a, 'html.parser')  
    data = soup.find_all('h2', class_='search-results__item__title')
    
#    if data == None:
#        time.sleep(5)
#        a = requests.get(url).text
#        soup = BeautifulSoup(a, 'html.parser')  
#        data = soup.find_all('h2', class_='search-results__item__title')
#    
    # CREATES STR OF TAG OBJECT
    for k,x in enumerate(data):
        boi.append(str(data[k]))
    
    # CLEANS UP THE DATA
    for k,x in enumerate(boi):
        boi[k] = boi[k].replace('"', '')
        boi[k] = boi[k].replace('<h2 class=search-results__item__title>', '')
        boi[k] = boi[k].replace('<a href=https://www.allabolag.se/', '')
        boi[k] = boi[k].replace('</a>', '')
        boi[k] = boi[k].replace('</h2>', '')
        boi[k] = boi[k].split('/')
        boi[k][0] = boi[k][0].replace('\n', '')
        boi[k][1] = boi[k][1].split('>')
        boi[k].append(boi[k][1][0])
        boi[k].append(boi[k][1][1])
        del boi[k][1]
        boi[k][2] = boi[k][2].replace('\n', '')
    
    # CREATES NESTED DICTIONARY
    for k,x in enumerate(boi):
        database[index] = {}
        database[index]['org_nr'] = boi[k][0]
        database[index]['url_name'] = boi[k][1]
        database[index]['name'] = boi[k][2]
        database[index]['url'] = url_str + database[index]['org_nr'] + '/' + database[index]['url_name'] 
        index += 1
        
    return database

        
with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
    future_to_url = {executor.submit(scraper, urls[kx]): kx for kx, url in enumerate (urls)}
    for future in concurrent.futures.as_completed(future_to_url):
        url = future_to_url[future]
        output2[url] = future.result()
        print(len(output2))
#%%
    
for k,x in enumerate(output):
    if bool(output[k]) == False:
        failed.append(urls[k])


# PICKELS DATA
#boi = []
#for kx,ky in enumerate(x):
#    for ki, xi in enumerate(x[kx]):
#        boi.append(x[kx][ki])       
#pickle.dump(x, open( "data/allabolag_database.pkl", "wb" ))        
    