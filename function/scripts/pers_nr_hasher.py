# -*- coding: utf-8 -*-
"""
Created on Sun Apr 22 13:24:30 2018

@author: carlelg
"""
import pickle
import hashlib
import itertools
from itertools import product
from string import ascii_letters, digits

month = '01'
day = '0123'
months = []
days = []
three_digit = []
years = []  
pers_nr = []



#YEARS
for i in product(digits, digits):
    years.append(''.join(i))

#MONTHS    
for i in product(month, digits):
    if int((''.join(i))) != 0:
        if int((''.join(i))) < 13:
            months.append(''.join(i))
            
#DAYS
for i in product(day, digits):
    if int((''.join(i))) != 0:
        if int((''.join(i))) < 32:
            days.append(''.join(i))
    
#THREE DIGITS
for i in product(digits, repeat=3):   
    three_digit.append(''.join(i))

# GENERATES ALL AVIALIBLE PERS NR
for k,x in enumerate(years):
    for k2,x2 in enumerate(months):     
        for k3,x3 in enumerate(days): 
            for k4,x4 in enumerate(three_digit):
                pers_nr.append(years[k] + months[k2] + days[k3] + three_digit[k4])


     
#%%

## SISTA SIFFRAN I PERSONNR    
#tex = '640823323'
          
for k,x in enumerate(pers_nr):
    
    tex = pers_nr[k]
    #tex ='010124305' 
    #tex = '550218274'               
    apa = list(tex)
    apa2 = []
    count = 2
    final = 0
     
    for ki,xi in enumerate(apa):
        apa[ki] = int(apa[ki])    
        apa2.append(apa[ki]*count)
        count += 1
        if count > 2:
            count = 1
        
    for ki,xi in enumerate(apa2):
        if apa2[ki] >= 10:
            apa2[ki] = list(str(apa2[ki]))
            apa2.append(int(apa2[ki][0]))
            apa2.append(int(apa2[ki][1]))
            del apa2[ki]
    
    for ki,xi in enumerate(apa2):        
        final += apa2[ki]
        
    
       
    if int(final) > 10:
        final = list(str(final))
        if len(final) > 1:
            final = 10 - int(final[1])
    else:
        final = list(str(final))
        final = 10 - int(final[0])
    
    if final == 10:
        final = 0
    
    final = str(final)

    pers_nr[k] = pers_nr[k] + final

        
#%%

pers_nr_hash = {}    
for k,x in enumerate(pers_nr):
    pers_hr_hash[(hashlib.md5(pers_nr[k].encode('utf-8')).hexdigest())] = pers_nr[k]

#pickle.dump(pers_nr_hash, open("data/pers_nr_hash.pkl", "wb" ) )