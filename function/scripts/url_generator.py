# -*- coding: utf-8 -*-
"""
Created on Tue May  1 12:03:51 2018

@author: carlelg
"""

from string import ascii_lowercase
import string
import pickle

import pandas as pd

from database_functions import scraper
from database_functions import page_generator


search_options = {'tp': 0, 'bransch': 1, 'omsattning': 2, 'ort': 3}
search_options_url = {0: 'xb', 1: 'xv', 2: 'xo', 3: 'xl'}
bolags_categories = pd.read_csv('data/bolags_categories.txt', sep='|', encoding='utf-8')


def contains_digits(s):
    return any(char.isdigit() for char in s)
#%%

inaktiv = ['', 'xs/']

ascii2 = []
ascii2.append('')
a = list(ascii_lowercase)

for k,x in enumerate(a):
    ascii2.append(a[k])

a = list(string.digits)

for k,x in enumerate(a):
    ascii2.append(a[k])



#%%






element = {}
final = []
boi = []

for kl,xl in enumerate(ascii_lowercase):
    for kl2, xl2 in enumerate(ascii2):
        for kl3, xl3 in enumerate(inaktiv):
            category = 0
            search_item = ascii_lowercase[kl] + ascii2[kl2]
            url = 'https://www.allabolag.se/where/' + inaktiv[kl3] + search_item + '/'# + 'page=9'
            result = scraper(url, category)
            result2 = {}
            
            for k,x in enumerate(result):
                result2[k] = result[k]
                
            
            # CREATES DICTIONARY
            element[search_item] = {}
            element[search_item] = result2
            
            
            
            #def generator(result, dic):
            #kx = 0
            sok = element[search_item]
            #result = sok[0]
            dic = sok[1]
            #sok['pages'] = []   
            pages = []
            
            
            for ko, xo in enumerate(dic):
                category = 0
                dic1 = dic[ko]  
                url2 = url + search_options_url[dic1[2]] + '/' + str(dic1[3])    
                
                
                # SMALLER THAN
                if dic1[1] < 8000:
                    nr_pages = dic1[1]//20 + 1 
                    
                    for i in range(nr_pages):
                        options_url = dic1[2]
                        category_url = dic1[3]
                        pages.append(url + search_options_url[options_url] + '/' + category_url + '/page/' + str(i + 1))
                        
                # GREATER THAN    
                if dic1[1] > 8000:
                    category += 1  
                    something = scraper(url2, category)
                
                    for kx, ky in enumerate(something[1]):
                        something1 = something[1][kx]
                        
                        # SMALLER THAN
                        if something1[1] <= 8000:
                            nr_pages = something1[1]//20 + 1 
                            for i in range(nr_pages):
                                options_url = something1[2]
                                category_url = something1[3]
                                pages.append(url2 + '/' + search_options_url[options_url] + '/' + category_url + '/page/' + str(i + 1))
                                          
                        # GREATER THAN
                        if something1[1] > 8000:
                            category += 1  
                            options_url = something1[2]
                            category_url = something1[3]
                            url3 = url2 + '/' +  search_options_url[options_url] + '/' + category_url
                            something2 = scraper(url3, category)
                                      
                            for kx1, ky in enumerate(something2[1]):
                                something3 = something2[1][kx1]
                                nr_pages = something3[1]//20 + 1 
                                options_url = something3[2]
                                category_url = something3[3]
                                
                                # SMALLER THAN
                                if something3[1] <= 8000:
                                    for i in range(nr_pages):
                                        pages.append(url3 + '/' + search_options_url[options_url ] + '/' + category_url + '/page/' + str(i + 1))
                                    
                                # GREATER THAN
                                if something3[1] > 8000:
                                    category += 1
                                    options_url = something3[2]
                                    category_url = something3[3]
                                    url4 = url3 + '/' +  search_options_url[options_url] + '/' + category_url
                                    something4 = scraper(url4, category)
                                    
                                    for kx2, ky2 in enumerate(something4[1]):
                                        something5 = something4[1][kx2]
                                        
                                        
                                        # SMALLER THAN
                                        if something5[1] <= 8000:
                                            options_url = something5[2]
                                            category_url = something5[3]
                                            nr_pages = something5[1]//20 + 1 
                                            for i in range(nr_pages):
                                                pages.append(url4 + '/' + search_options_url[options_url] + '/' + category_url + '/page/' + str(i + 1))
                                                 
                                        # GREATER THAN
                                        if something5[1] > 8000:
                                            for i in range(8000):
#                                                category += 1
                                                options_url = something5[2]
                                                category_url = something5[3]
                                                url5 = url4 + '/' + search_options_url[options_url] + '/' + category_url + '/page/' + str(i + 1)
                                                pages.append(url4 + '/' + search_options_url[options_url] + '/' + category_url + '/page/' + str(i + 1))
    #                                        something6 = scraper(url2, category)
            final.append(pages)


urls2 = []            
for k,x in enumerate(final):
    for kx, ky in enumerate(final[k]):
        urls2.append(final[k][kx])