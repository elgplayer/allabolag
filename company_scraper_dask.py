# -*- coding: utf-8 -*-
"""
Created on Tue Apr 24 20:37:45 2018

@author: carlelg
"""

# -*- coding: utf-8 -*-
"""
Created on Sun Apr 15 21:27:32 2018

@author: carlelg
"""

import pickle
from datetime import datetime
import random
import os
from multiprocessing.pool import ThreadPool

import requests
import cfscrape
from bs4 import BeautifulSoup
import pandas as pd
import dask
from dask import delayed, compute
import dask.multiprocessing


from function.misc_functions import company_scraper

from_num = 0
to_num = 1000




#database_big = pickle.load(open("data/save.pkl", "rb" ))
#database_big = database_big[from_num:to_num]

#pool = ThreadPool()
#dask.config.set(pool=pool) 
#results = pool.map(company_scraper, range(from_num, to_num))


#data = [dask.delayed(company_scraper)(id) for id in range(100)]
#df = dask.delayed(data)
#df = df.compute()

jobs = [delayed(company_scraper)(id) for id in range(100)]
compute(*jobs, get=dask.multiprocessing.get)
#close the pool and wait for the work to finish 
# pool.close() 
# pool.join() 